#!/usr/bin/env bash
source ~/.bashrc
roslaunch turtlebot3_bringup turtlebot3_robot.launch &>/dev/null &
sleep 10
roslaunch turtlebot3_bringup turtlebot3_rpicamera.launch &>/dev/null &
sleep 20
