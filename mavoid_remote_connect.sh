#!/usr/bin/env bash
remoteIP=$(ifconfig | grep 'inet ' | grep -v '127.0.0.1' | sed -n -e 's/^.*inet //p' | sed -n -e 's/\s.*//p')
sed -i "s/ROS_MASTER_URI=.*/ROS_MASTER_URI=http:\/\/$remoteIP:11311/" ~/.bashrc
sed -i "s/ROS_HOSTNAME=.*/ROS_HOSTNAME=$remoteIP/" ~/.bashrc
source ~/.bashrc

if [ $# -eq 0 ]
then
    turtlebot3Name=$(cat ./mavoid/mavoid_remote_config.txt | grep 'TURTLEBOT3_DEFAULT_NAME' | sed -n -e 's/^.*TURTLEBOT3_DEFAULT_NAME=//p')
else
    turtlebot3Name=$1
fi

turtlebotIP=$(ping -c 1 $turtlebot3Name | cut -d'(' -f 2 | cut -d')' -f 1 | head -n 1)

./mavoid/mavoid_remote_start $turtlebotIP $remoteIP
