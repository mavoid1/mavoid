#!/usr/bin/env bash
source ~/.bashrc
roslaunch turtlebot3_navigation turtlebot3_navigation.launch map_file:=$HOME/sprint4/map.yaml &>/dev/null &
sleep 10;
echo "Launched Navigation"
